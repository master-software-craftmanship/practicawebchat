package es.sidelab.webchat;

import static java.lang.Thread.sleep;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.*;
import java.util.concurrent.*;

import org.junit.Test;

import es.codeurjc.webchat.Chat;
import es.codeurjc.webchat.ChatManager;
import es.codeurjc.webchat.User;

public class ChatManagerTest {
	private ChatManager chatManager;
	private ExecutorService executor;

	@Test
	public void newChat() throws InterruptedException, TimeoutException {

		// Crear el chat Manager
		chatManager = new ChatManager(5);

		// Crear un usuario que guarda en chatName el nombre del nuevo chat
		final String[] chatName = new String[1];

		addUserToChatManagerAndReturnIt(new TestUser("user") {
			public void newChat(Chat chat) {
				chatName[0] = chat.getName();
			}
		});

		// Crear un nuevo chat en el chatManager
		chatManager.newChat("Chat", 5, TimeUnit.SECONDS);

		// Comprobar que el chat recibido en el método 'newChat' se llama 'Chat'
		assertEquals("The method 'newChat' should be invoked with 'Chat', but the value is "
				+ chatName[0], "Chat", chatName[0]);
	}

	@Test
	public void chatClosed() throws TimeoutException, InterruptedException {
		// GIVEN a chatManager
		chatManager = new ChatManager(5);
		final int numberOfUsers = 3;
		final CountDownLatch countDownLatch = new CountDownLatch(numberOfUsers);
		final ConcurrentMap<String, String> closedChatMessages = new ConcurrentHashMap<>();
		final String chatName = "chatName";
		// with one chat
		final Chat chat = chatManager.newChat(chatName, 5, TimeUnit.SECONDS);
		// and 3 users in it
		for (int i=0; i<numberOfUsers; i++) {
			final String userName = "user" + i;
			chat.addUser(addUserToChatManagerAndReturnIt(createCustomChatClosedTestUser(userName, (aChat)->{
				try {
					closedChatMessages.put(userName, "chat " + aChat.getName() + " closed");
					// each of them with a slow connection
					sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
					throw new RuntimeException();
				} finally {
					countDownLatch.countDown();
				}
			})));
		}
		// WHEN the chat is closed
		chat.close();
		// THEN all the users receive the chatClosed message in parallel
		// A modification is done to the close chat procedure so the notification is done in parallel.
		// If sequential processing is done, more than 1 second is consumed to close the chat.
		assertTrue(countDownLatch.await(1, TimeUnit.SECONDS));
		for(int i=0; i<numberOfUsers; i++) {
			assertThat(closedChatMessages, hasKey("user" + i));
			assertThat(closedChatMessages.get("user" + 1), is("chat " + chatName + " closed"));
		}
	}

	@Test
	public void newUserInChat() throws InterruptedException, TimeoutException {

		chatManager = new ChatManager(5);

		final String[] newUser = new String[1];

		Semaphore newUserReceived = new Semaphore(0);
		User user1 = addUserToChatManagerAndReturnIt(new TestUser("user1") {
			@Override
			public void newUserInChat(Chat chat, User user) {
				newUser[0] = user.getName();
				newUserReceived.release();
			}
		});

		User user2 = addUserToChatManagerAndReturnIt(new TestUser("user2"));

		Chat chat = chatManager.newChat("Chat", 5, TimeUnit.SECONDS);

		chat.addUser(user1);
		chat.addUser(user2);

		assertTrue(newUserReceived.tryAcquire(100, TimeUnit.MILLISECONDS));
		assertTrue("Notified new user '" + newUser[0] + "' is not equal than user name 'user2'",
				"user2".equals(newUser[0]));

	}

	@Test
	public void userExitsFromChat() throws TimeoutException, InterruptedException {
		// GIVEN a chatManager
		chatManager = new ChatManager(5);
		final int numberOfUsers = 3;
		final CountDownLatch countDownLatch = new CountDownLatch(numberOfUsers);
		final ConcurrentMap<String, String> userExitedChatMessages = new ConcurrentHashMap<>();
		final String chatName = "chatName";
		// and a chat
		final Chat chat = chatManager.newChat(chatName, 5, TimeUnit.SECONDS);
		final String exitingUserName = "exitingUser";
		// with an user that will exit the chat
		final User exitingUser = addUserToChatManagerAndReturnIt(new TestUser(exitingUserName));
		chat.addUser(exitingUser);
		// and numberOfUsers additional users
		for (int i=0; i<numberOfUsers; i++) {
			final String userName = "user" + i;
			chat.addUser(addUserToChatManagerAndReturnIt(createCustomUserExitedFromChatTestUser(userName, (aChat, aUser)->{
				try {
					userExitedChatMessages.put(userName, "user " + aUser.getName() + " exited from chat " + aChat.getName());
					// each with a slow connection
					sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
					throw new RuntimeException();
				} finally {
					countDownLatch.countDown();
				}
			})));
		}
		// WHEN the exitingUser exits from the chat
		chat.removeUser(exitingUser);
		// THEN all the remaining users receive the userExitedFromChat message in parallel
		assertTrue(countDownLatch.await(1, TimeUnit.SECONDS));
		for(int i=0; i<numberOfUsers; i++) {
			assertThat(userExitedChatMessages, hasKey("user" + i));
			assertThat(userExitedChatMessages.get("user" + 1), is("user " + exitingUserName + " exited from chat " + chatName));
		}
	}

	@Test
	public void parallelUserSimulationNoExceptionsTest() throws InterruptedException, ExecutionException {
		// GIVEN a chat manager with 50 max chats
		chatManager = new ChatManager(50);
		// and 4 concurrent simulated users
		executor = Executors.newFixedThreadPool(4);
		CompletionService<String> completionService = new ExecutorCompletionService<>(executor);
		for(int i=0; i<4; i++) {
			//WHEN users are simulated
			completionService.submit(getUserSimulationCallable("user" + i));
		}

		//THEN no exceptions are elevated
		for (int i=0; i<4; i++) {
			try {
				Future<String> f = completionService.take();
				f.get();
			} catch (ExecutionException e) {
				e.printStackTrace();
				throw e;
			} catch (Exception e) {
				throw e;
			}
		}
	}

	@Test
	public void parallelNotificationTest() throws InterruptedException, TimeoutException {
		final int receivingUserNumber = 3;
		final String testMessage = "test message";

		// GIVEN
		// Chat manager with 1 chat max
		chatManager = new ChatManager(1);
		CountDownLatch countDownLatch = new CountDownLatch(receivingUserNumber);
		// And receivingUserNumber of users with slow new message processing time in chat
		Chat testChat = chatManager.newChat("Chat", 5, TimeUnit.SECONDS);
		for(int i=0; i<receivingUserNumber; i++) {
			testChat.addUser(addUserToChatManagerAndReturnIt(
					createCustomNewMessageTestUser("user" + i,
						(chat, user, message)->{
							try {
								sleep(1000);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
							countDownLatch.countDown();
						})));
		}

		// WHEN
		// a new user sends a message
		User sendingUser = addUserToChatManagerAndReturnIt(new TestUser("sendingUser"));
		testChat.addUser(sendingUser);
		testChat.sendMessage(sendingUser, testMessage);

		// THEN all users in chat receive the message in parallel
		assertTrue(countDownLatch.await(2, TimeUnit.SECONDS));
	}

	@Test
	public void messageOrderTest() throws InterruptedException, TimeoutException {
		final int numberOfMessages = 5;
		//GIVEN a chat manager with one chat
		chatManager = new ChatManager(1);
		// a message sending user
		User sendingUser = addUserToChatManagerAndReturnIt(new TestUser("sendingUser"));
		// a chat with such user added
		Chat testChat = createChatAndAddUserToIt("Chat", sendingUser);
		// a list of messages
		List<String> testingMessages = new ArrayList<>(numberOfMessages);
		for (int i=0; i<numberOfMessages; i++) {
			testingMessages.add(String.valueOf(i));
		}

		Exchanger<List<String>> exchanger = new Exchanger<>();
		List<String> receivedMessages = new ArrayList<>(numberOfMessages);
		// and a slow new message processing user added to the same chat
		testChat.addUser(addUserToChatManagerAndReturnIt(createCustomNewMessageTestUser("receivingUser",
				(chat, user, message)->{
					try {
						sleep(100);
						receivedMessages.add(message);
						if (receivedMessages.size() == numberOfMessages) {
							exchanger.exchange(receivedMessages);
						}
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				})));

		//WHEN all messages sent to the receiving user
		for (String message : testingMessages) {
			testChat.sendMessage(sendingUser, message);
		}
		//THEN messages received list is exactly the same as the sent one
		assertThat(exchanger.exchange(new ArrayList<>(), 1, TimeUnit.SECONDS), is(testingMessages));
	}

	@Test(expected = TimeoutException.class)
	public void newChatTimeoutElevated() throws InterruptedException, TimeoutException {
		//GIVEN a chat manager with 1 max chats
		chatManager = new ChatManager(1);
		try {
			//WHEN chat is full
			chatManager.newChat("Chat1", 1, TimeUnit.MILLISECONDS);
			//THEN same create same chat as existent does not wait
			chatManager.newChat("Chat1", 1, TimeUnit.MILLISECONDS);
		} catch (TimeoutException e) {
			e.printStackTrace();
			assertTrue(false);
		}
		//and new chat waits until timeout
		chatManager.newChat("Chat2", 100, TimeUnit.MILLISECONDS);
	}

	@Test
	public void newChatWaits() throws TimeoutException, InterruptedException {
		executor = Executors.newFixedThreadPool(1);
		//GIVEN a chat manager with 1 max chats
		chatManager = new ChatManager(1);
		// and a chat
		Chat chat = chatManager.newChat("Chat1", 1, TimeUnit.MILLISECONDS);
		// that will be closed after 500 millis
		executor.submit(() -> {
			try {
				sleep(500);
				chatManager.closeChat(chat);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		});
		final long start = System.nanoTime();
		//WHEN new chat is created with 1 second timeout
		chatManager.newChat("Chat2", 1, TimeUnit.SECONDS);
		final long microseconds = (System.nanoTime() - start) / 1000;
		//THEN no exception will be elevated and at least 500 millis will be waited until the manager has space for the chat
		assertThat("chat creation wait time", microseconds, greaterThan(500L));
	}

	@Test
	public void GivenFullManagerWhenTwoNewChatsAndOneGapBeforeTimeoutOneChatGetsItTheOtherElevatesException() throws TimeoutException, InterruptedException, ExecutionException {
		executor = Executors.newFixedThreadPool(4);
		//GIVEN a chat manager with 1 max chats
		chatManager = new ChatManager(1);
		// and a chat
		Chat chat = chatManager.newChat("Chat1", 1, TimeUnit.MILLISECONDS);
		// that will be closed after 500 millis
		executor.submit(() -> {
			try {
				sleep(500);
				chatManager.closeChat(chat);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		});
		CompletionService<Chat> completionService = new ExecutorCompletionService<>(executor);
		//WHEN two chats with same name are created
		Future<Chat> firstChat2future = completionService.submit(()-> chatManager.newChat("Chat2", 1, TimeUnit.SECONDS));
		// and the second chat is null if a Timeout is elevated
		Future<Chat> secondChat2future = completionService.submit(()-> {
			try {
				return chatManager.newChat("Chat2", 1, TimeUnit.SECONDS);
			} catch (TimeoutException e) {
				return null;
			}
		});

		final long start = System.nanoTime();
		//THEN the first chat is correctly created
		assertThat(firstChat2future.get(), notNullValue());
		final long microseconds = (System.nanoTime() - start) / 1000;
		// and no exception is elevated and at least 500 millis are waited until the manager has space for the chat
		assertThat("chat creation wait time", microseconds, greaterThan(500L));
		// and the second chat is null because the Timeout exception is elevated
		assertThat(secondChat2future.get(), nullValue());
	}

	private Callable<String> getUserSimulationCallable(String userName) {
		final int numberOfTestChats = 5;
		return () -> {
			try {
				User user = addUserToChatManagerAndReturnIt(new TestUser(userName));
				for (int i=0; i<numberOfTestChats; i++) {
					printUsersInChat(createChatAndAddUserToIt("Chat" + i, user));
				}
				return "";
			} catch (InterruptedException e) {
				throw new IllegalStateException("task interrupted", e);
			}
		};
	}

	private User addUserToChatManagerAndReturnIt(User user) {
		chatManager.newUser(user);
		return user;
	}

	private Chat createChatAndAddUserToIt(String chatName, User user) throws InterruptedException, TimeoutException {
		Chat chat = chatManager.newChat(chatName, 5, TimeUnit.SECONDS);
		chat.addUser(user);
		return chat;
	}

	private void printUsersInChat(Chat chat) {
		Collection<User> usersInChat = chat.getUsers();
		for (User u : usersInChat) {
			System.out.println(u.getName());
		}
	}

	private User createCustomNewMessageTestUser(String userName, NewMessageMethod newMessageMethod) {
		return new TestUser(userName) {
			@Override
			public void newMessage(Chat chat, User user, String message) {
				newMessageMethod.newMessage(chat, user, message);
			}
		};
	}

	private User createCustomChatClosedTestUser(String userName, ChatClosedMethod chatClosedMethod) {
		return new TestUser(userName) {
			@Override
			public void chatClosed(Chat chat) {
				chatClosedMethod.chatClosed(chat);
			}
		};
	}

	private User createCustomUserExitedFromChatTestUser(String userName, UserExitedFromChatMethod userExitedFromChatMethod) {
		return new TestUser(userName) {
			@Override
			public void userExitedFromChat(Chat chat, User user) {
				userExitedFromChatMethod.userExitedFromChat(chat, user);
			}
		};
	}

	private interface NewMessageMethod {
		void newMessage(Chat chat, User user, String message);
	}

	private interface ChatClosedMethod {
		void chatClosed(Chat chat);
	}

	private interface UserExitedFromChatMethod {
		void userExitedFromChat(Chat chat, User user);
	}
}

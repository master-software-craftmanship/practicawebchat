package es.codeurjc.webchat;

import javafx.util.Pair;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Chat {

	private String name;
	private ConcurrentMap<String, Pair<User, ExecutorService>> users = new ConcurrentHashMap<>();

	private ChatManager chatManager;

	public Chat(ChatManager chatManager, String name) {
		this.chatManager = chatManager;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void addUser(User user) {
		ExecutorService executorService = Executors.newFixedThreadPool(1);
		Pair<User, ExecutorService> existingUser = users.putIfAbsent(user.getName(), new Pair<>(user, executorService));
		if (null == existingUser) {
			notifyNewUserInChat(user);
		}
	}

	private void notifyNewUserInChat(User user) {
		for (Pair<User, ExecutorService> u : users.values()) {
			if (u.getKey() != user) {
				u.getValue().submit(() -> u.getKey().newUserInChat(this, user));
			}
		}
	}

	public void removeUser(User user) {
		Pair<User, ExecutorService> removedUser = users.remove(user.getName());
		if (null != removedUser) {
			notifyUserExitedFromChat(removedUser.getKey());
		}
	}

	private void notifyUserExitedFromChat(User user) {
		for (Pair<User, ExecutorService> u : users.values()) {
			u.getValue().submit(() -> u.getKey().userExitedFromChat(this, user));
		}
	}

	public Collection<User> getUsers() {
		List<User> existingUsers = new ArrayList<>(users.size());
		for (Pair<User, ExecutorService> user : users.values()) {
			existingUsers.add(user.getKey());
		}
		return Collections.unmodifiableCollection(existingUsers);
	}

	public User getUser(String name) {
		return users.get(name).getKey();
	}

	public void sendMessage(User user, String message) {
		for(Pair<User, ExecutorService> u : users.values()){
			u.getValue().submit(() -> u.getKey().newMessage(this, user, message));
		}
	}

	public void close() {
		for(Pair<User, ExecutorService> u : users.values()){
			u.getValue().submit(() -> u.getKey().chatClosed(this));
		}
		this.chatManager.closeChat(this);
	}
}

package es.codeurjc.webchat;

import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.*;

public class ChatManager {

	private ConcurrentMap<String, Chat> chats = new ConcurrentHashMap<>();
	private ConcurrentMap<String, User> users = new ConcurrentHashMap<>();
	private Semaphore chatsCounter;

	public ChatManager(int maxChats) {
		this.chatsCounter = new Semaphore(maxChats);
	}

	public void newUser(User user) {
		User existingUser = users.putIfAbsent(user.getName(), user);
		if(user.equals(existingUser)){
			throw new IllegalArgumentException("There is already a user with name \'"
					+ user.getName() + "\'");
		}
	}

	private Chat createChatAndNotifyIfNotFull(String name) {
		if (chatsCounter.tryAcquire()) {
			return createChatAndNotifyUsers(name);
		} else {
			return null;
		}
	}

	private Chat createChatAndNotifyUsers(String name) {
		Chat newChat = new Chat(this, name);
		for (User user : users.values()) {
			user.newChat(newChat);
		}
		return newChat;
	}

	public Chat newChat(String name, long timeout, TimeUnit unit) throws InterruptedException,
			TimeoutException {
		Chat addedChat = chats.computeIfAbsent(name, this::createChatAndNotifyIfNotFull);
		if (null == addedChat) {
			if (chatsCounter.tryAcquire(timeout, unit)) {
				return chats.computeIfAbsent(name, this::createChatAndNotifyUsers);
			} else {
				throw new TimeoutException("There is no enough capacity to create a new chat");
			}
		} else {
			return addedChat;
		}
	}

	public void closeChat(Chat chat) {
		Chat removedChat = chats.remove(chat.getName());
		if (removedChat == null) {
			throw new IllegalArgumentException("Trying to remove an unknown chat with name \'"
					+ chat.getName() + "\'");
		}
		chatsCounter.release();
	}

	public Collection<Chat> getChats() {
		return Collections.unmodifiableCollection(chats.values());
	}

	public Chat getChat(String chatName) {
		return chats.get(chatName);
	}

	public Collection<User> getUsers() {
		return Collections.unmodifiableCollection(users.values());
	}

	public User getUser(String userName) {
		return users.get(userName);
	}

	public void close() {}
}
